// database/firebaseDb.js
import { initializeApp } from "firebase/app";
import {getFirestore} from "firebase/firestore";
import {getAuth} from 'firebase/auth'
const firebaseConfig = {
    apiKey: "AIzaSyCIL9uEESyCffa_c22gFvrmjbLYtaFBabo",
    authDomain: "react-native-firebase-ede2c.firebaseapp.com",
    databaseURL: "https://react-native-firebase-ede2c.firebaseio.com",
    projectId: "react-native-firebase-ede2c",
    storageBucket: "react-native-firebase-ede2c.appspot.com",
    messagingSenderId: "665909486391",
    appId: "1:665909486391:android:db958dce9f1f8ddc2eb0b2"
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)
export default getFirestore();