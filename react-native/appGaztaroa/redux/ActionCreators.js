import * as ActionTypes from './ActionTypes';
import { onSnapshot, collection, Firestore, setDoc, doc } from "firebase/firestore"
import db from "../firebaseDB";

export const postComentario = (idComentario, excursionId, autor, comentario, dia, valoracion) => (dispatch) => {
    let comentarioObj = {
        id: idComentario,
        autor: autor,
        excursionId: excursionId,
        comentario: comentario,
        dia: dia,
        valoracion: valoracion
    }
    setDoc(doc(db, "Comentarios", idComentario + "_" + autor), comentarioObj)
    dispatch(addComentario(comentarioObj))
};
export const addComentario = (comentarioObj) => ({
    type: ActionTypes.POST_COMENTARIO,
    payload: comentarioObj
});


export const fetchComentarios = () => (dispatch) => {
    onSnapshot(collection(db, "Comentarios"), (snapshot) => {
        let comentarios = snapshot.docs.map((doc) => ({ ...doc.data(), id: doc.data().id }));
        dispatch(addComentarios(comentarios));
    });
};

export const comentariosFailed = (errmess) => ({
    type: ActionTypes.COMENTARIOS_FAILED,
    payload: errmess
});

export const addComentarios = (comentarios) => ({
    type: ActionTypes.ADD_COMENTARIOS,
    payload: comentarios
});

export const  fetchExcursiones = () => (dispatch) => {
    dispatch(excursionesLoading());
    onSnapshot(collection(db, "Excursiones"),(snapshot) => {
        let excursiones = snapshot.docs.map((doc) => ({ ...doc.data(), id: doc.data().id }));
        dispatch(addExcursiones(excursiones));
    });
};

export const excursionesLoading = () => ({
    type: ActionTypes.EXCURSIONES_LOADING
});

export const excursionesFailed = (errmess) => ({
    type: ActionTypes.EXCURSIONES_FAILED,
    payload: errmess
});

export const addExcursiones = (excursiones) => ({
    type: ActionTypes.ADD_EXCURSIONES,
    payload: excursiones
});

export const fetchCabeceras = () => (dispatch) => {
    dispatch(cabecerasLoading());
    onSnapshot(collection(db, "Cabeceras"), (snapshot) => {
        let cabeceras = snapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
        dispatch(addCabeceras(cabeceras))
    });
};

export const cabecerasLoading = () => ({
    type: ActionTypes.CABECERAS_LOADING
});

export const cabecerasFailed = (errmess) => ({
    type: ActionTypes.CABECERAS_FAILED,
    payload: errmess
});

export const addCabeceras = (cabeceras) => ({
    type: ActionTypes.ADD_CABECERAS,
    payload: cabeceras
});

export const fetchActividades = () => (dispatch) => {
    dispatch(actividadesLoading());
    onSnapshot(collection(db, "Actividades"), (snapshot) => {
        let actividades = snapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
        dispatch(addActividades(actividades))
    });
};

export const actividadesLoading = () => ({
    type: ActionTypes.ACTIVIDADES_LOADING
});

export const actividadesFailed = (errmess) => ({
    type: ActionTypes.ACTIVIDADES_FAILED,
    payload: errmess
});

export const addActividades = (actividades) => ({
    type: ActionTypes.ADD_ACTIVIDADES,
    payload: actividades
});

export const postFavorito = (excursionId) => (dispatch) => {
    setTimeout(() => {
        console.log(excursionId);
        //dispatch(addFavorito(excursionId));
    }, 2000);
};
export const addFavorito = (excursionId) => ({
    type: ActionTypes.POST_FAVORITO,
    payload: excursionId
});
