import React, { Component } from "react";
import { StyleSheet, ImageBackground, Text, ScrollView, View } from "react-native";
import { Card } from "react-native-elements";
import { connect } from 'react-redux';
import { baseUrl } from '../comun/comun';
import IndicadorActividad from './IndicadorActividadComponent'
import NetInfo from '@react-native-community/netinfo';


const mapStateToProps = (state) => {
  return {
    excursiones: state.excursiones,
    cabeceras: state.cabeceras,
    actividades: state.actividades,
  };
};

function RenderItem(props) {
  NetInfo.fetch().then(state => {
    if(!state.type  == "cellular") {
      alert("Cuidado! No estás conectado a ninguna red Wi-Fi, no gastes datos");
    }
  });
  const styles = StyleSheet.create({
    coverImage: {
      width: '100%',
      height: 200,
    },
    textView: {
      position: 'absolute',
      alignItems: 'center',
      top: 20,
      left: 0,
      right: 0,
      bottom: 0,
    },
    imageText: {
      fontSize: 30,
      color: 'chocolate',
      fontWeight: 'bold',
    },
  });
    if (props.isLoading) {
      return (
        <IndicadorActividad />
      );
    }
    else if (props.errMess) {
      return (
        <View>
          <Text>{props.errMess}</Text>
        </View>
      );
    } else {
    const item = props.item;
    
    if (item != null && item != undefined) {
      return (
        <Card>
          <ImageBackground style={styles.coverImage} source={{ uri: item.imagen }}>
            <View style={styles.textView}>
              <Text style={styles.imageText}>{item.nombre}</Text>
            </View>
          </ImageBackground>
          <Text style={{ margin: 20 }}>{item.descripcion}</Text>
        </Card>
      );
    } else {
      return <View></View>;
    }
  }


}

class Home extends Component {

  render() {
    return (
      <ScrollView>
        <RenderItem
          item={
            this.props.cabeceras.cabeceras.filter((cabecera) => cabecera.destacado)[0]
          }
        />
        <RenderItem
          item={
            this.props.excursiones.excursiones.filter((excursion) => excursion.destacado)[0]
          }
          isLoading={this.props.excursiones.isLoading}
          errMess={this.props.excursiones.errMess}
        />
        <RenderItem
          item={
            this.props.actividades.actividades.filter((actividad) => actividad.destacado)[0]
          }
        />
      </ScrollView>
    );
  }
}

export default connect(mapStateToProps)(Home);

