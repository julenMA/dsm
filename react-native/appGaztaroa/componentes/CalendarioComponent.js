import React, { Component } from 'react';
import { ListItem, Avatar } from 'react-native-elements';
import { SafeAreaView, FlatList } from 'react-native';
import { colorGaztaroaClaro, baseUrl } from '../comun/comun';
import { connect } from 'react-redux';
import { IndicadorActividad } from './IndicadorActividadComponent';
import { View } from 'react-native';
const mapStateToProps = state => {
  return {
    excursiones: state.excursiones
    }
  }
  class Calendario extends Component {
  render() {

    const { navigate } = this.props.navigation;

    const renderCalendarioItem = ({ item, index }) => {
      if (this.props.excursiones.isLoading) {
        return (
          <IndicadorActividad />
        )
      } else {
        return (
          <View>
            <ListItem
              key={index}
              onPress={() => navigate('DetalleExcursion', { excursionId: item.id })}
              containerStyle={{ backgroundColor: generateColor() }}
              bottomDivider>
              <Avatar source={{ uri: item.imagen }} />
              <ListItem.Content>
                <ListItem.Title>{item.nombre}</ListItem.Title>
                <ListItem.Subtitle>{item.descripcion}</ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>

          </View>
        );
      };
    }


    const generateColor = () => {
      const randomColor = Math.floor(Math.random() * 16777215)
        .toString(16)
        .padStart(6, "0");
      return `#${randomColor}`;
    };

    return (

      <SafeAreaView>
        <FlatList
          isLoading={this.props.excursiones.isLoading}
          data={this.props.excursiones.excursiones}
          renderItem={renderCalendarioItem}
          keyExtractor={item => item.id.toString()}
        />
      </SafeAreaView>
    );
  }
}

export default connect(mapStateToProps)(Calendario);
