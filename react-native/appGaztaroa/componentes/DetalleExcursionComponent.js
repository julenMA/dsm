
import React, { Component, useState } from "react";
import { Modal, StyleSheet, Text, Pressable, View, ScrollView, Share, TouchableOpacity } from "react-native";
import { Card, Icon, Rating, Input } from "react-native-elements";
import { connect } from 'react-redux';
import { postComentario, postFavorito,fetchComentarios } from "../redux/ActionCreators";

const mapStateToProps = state => {
  return {
    excursiones: state.excursiones,
    comentarios: state.comentarios,
    favoritos: state.favoritos
  }
}
const mapDispatchToProps = dispatch => ({
  fetchComentarios: () => dispatch(fetchComentarios()),
  postFavorito: (excursionId) => dispatch(postFavorito(excursionId)),
  postComentario: (idComentario, excursionId, autor, comentario, dia, valoracion) => 
            dispatch(postComentario(idComentario, excursionId, autor, comentario, dia, valoracion))
});

const url = 'https://firebasestorage.googleapis.com/v0/b/web-app-react-3bbfa.appspot.com/o/40A%C3%B1os.png?alt=media&token=0ab34d92-a933-4771-882c-11335829b5b4';
const onShare = async (titulo) => {
  try {
    const result = await Share.share({
      message:
        ("¡Visita " + titulo + "!" + '\n' + url)
    });
  } catch (error) {
    alert(error.message);
  }
};


function RenderExcursion(props) {
  const [modalVisible, setModalVisible] = useState(false);
  const [comentarioActual, setComentarioActual] = useState("");
  const [autorComentario, setAutorComentario] = useState("");
  const [valoracionComentario, setValoracionComentario] = useState(3);
  const excursion = props.excursion;
  const comentarios = props.comentarios;

  let enviarComentario = function(){
    const fecha = new Date();
    let enviar = props.enviarComentario;
    enviar(props.comentarios.length + 1, excursion.id, autorComentario, comentarioActual, fecha.toString(), valoracionComentario);
    let borrar = props.borrarComentario;
    setModalVisible(!modalVisible);
    borrar();
  }


  if (excursion != null) {
    return (
      <Card>
        <Card.Title>{excursion.nombre}</Card.Title>
        <Card.Divider />
        <Card.Image source={{ uri: excursion.imagen }}></Card.Image>
        <Text style={{ margin: 20 }}>{excursion.descripcion}</Text>
        <View style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center"
        }}>
          <Icon
            raised
            reverse
            name={props.favorita ? "heart" : "heart-o"}
            type="font-awesome"
            color="#f50"
            onPress={() =>
              props.favorita
                ? console.log("La excursión ya se encuentra entre las favoritas")
                : props.postFavorito()
            }
          />
          <Icon
            raised
            reverse
            name={"pencil"}
            type="font-awesome"
            color="#065ce5"
            onPress={() =>
              setModalVisible(true)
            }
          />
          <Icon
            raised
            reverse
            name={"share"}
            type="font-awesome"
            color="green"
            onPress={() =>
              onShare(excursion.nombre)
            }
          />
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
                <TouchableOpacity onPressOut={() => setModalVisible(!modalVisible)} activeOpacity={1} >
                  <Rating
                    showRating fractions="{1}"
                    startingValue="{3}"
                    onPress={value => setValoracionComentario(value)}
                    value={valoracionComentario}
                   />
                  <Input autoFocus
                    style={styles.input}
                    placeholder='Nombre'
                    leftIcon={{ type: 'font-awesome', name: 'user' }}
                    leftIconContainerStyle={{ marginLeft: -30 }}
                    onChangeText={value => setAutorComentario(value)}
                    value={autorComentario}
                    name="Nombre"
                  />
                  <Input autoFocus
                    style={{ marginLeft: '5%' }}
                    placeholder='Comentario'
                    leftIcon={{ type: 'font-awesome', name: 'comment' }}
                    leftIconContainerStyle={{ marginLeft: -30 }}
                    onChangeText={value => setComentarioActual(value)}
                    value={comentarioActual}
                    name="Comentario"
                  />

                  <Pressable
                    style={[styles.button, styles.buttonSend]}
                    onPress={() => enviarComentario()}
                  >
                    <Text style={styles.textStyle}>Enviar</Text>
                  </Pressable>
                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => setModalVisible(!modalVisible)}
                  >
                    <Text style={styles.textStyle}>Cancelar</Text>
                  </Pressable>
                </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </Card >
    );
  } else {
    return <View></View>;
  }
}






function RenderComentario(props) {
  const comentarios = props.comentarios;

  function RenderCommentarioItem({ item, index }) {
    return (
      <View key={index} style={{ margin: 10 }}>
        <Text style={{ fontSize: 14 }}>{item.comentario}</Text>
        <Text style={{ fontSize: 12 }}>{item.valoracion} Stars</Text>
        <Text style={{ fontSize: 12 }}>
          {"-- " + item.autor + ", " + item.dia}{" "}
        </Text>
      </View>
    );
  }
  return (
    <Card>
      <Card.Title>Comentarios</Card.Title>
      <Card.Divider />
      {comentarios.map((item, index) => (
        <RenderCommentarioItem key={index} item={item} index={index} />
      ))}
    </Card>
  );
}

class DetalleExcursion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valoracion: 3,
      autor: '',
      comentario: '',
      showModal: false
    }
  }
  marcarFavorito(excursionId) {
    this.setState({ favoritos: this.props.favoritos.favoritos.push(excursionId) });
    this.props.postFavorito(excursionId);

  }
  enviarComentario(idComentario, excursionId, autor, comentario, dia, valoracion){
    this.props.postComentario(idComentario, excursionId, autor, comentario, dia, valoracion);
    this.props.fetchComentarios();

  }  
  resetForm() {
    this.setState({
    valoracion: 3,
    autor: '',
    comentario: '',
    dia: '',
    showModal: false
    });
  }   
  render() {

    const { excursionId } = this.props.route.params;
    return (
      <ScrollView>
        <RenderExcursion
          excursion={this.props.excursiones.excursiones[excursionId]}
          favorita={this.props.favoritos.favoritos.some(el => el === excursionId)}
          postFavorito={() => this.marcarFavorito(excursionId)}
          enviarComentario={(idComentario, excursionId, autor, comentario, dia, valoracion) => 
            this.enviarComentario(idComentario, excursionId, autor, comentario, dia, valoracion)}
            borrarComentario = {() => this.resetForm()}
            comentarios={this.props.comentarios.comentarios}
        />
        <RenderComentario
          comentarios={this.props.comentarios.comentarios.filter(
            (comentario) => comentario.excursionId === excursionId
          )}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    marginTop: 22
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 55,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  input: {
    height: '100%',
    margin: 12,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonSend: {
    marginTop: '5%',
    backgroundColor: "#2196F3",
  },
  buttonClose: {
    marginTop: '5%',
    backgroundColor: "red",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});







export default connect(mapStateToProps, mapDispatchToProps)(DetalleExcursion);
