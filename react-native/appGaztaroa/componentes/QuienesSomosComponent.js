import React, { Component } from "react";
import { Text, View, ScrollView } from "react-native";
import { Card, ListItem, Avatar, Button } from "react-native-elements";
import { connect } from 'react-redux';
import { IndicadorActividad } from "./IndicadorActividadComponent";
import { MailComposerOptions, MailComposerResult, MailComposerStatus, composeAsync } from "expo-mail-composer";
        
const mapStateToProps = state => {
  return {
    actividades: state.actividades
  }
}

const INFO = [
  {
    id: 0,
    titulo: "Un poquito de historia",
    descripcion: `El nacimiento del club de montaña Gaztaroa se remonta a la primavera de 1976 cuando jóvenes aficionados a la montaña y pertenecientes a un club juvenil decidieron crear la sección montañera de dicho club. Fueron unos comienzos duros debido sobre todo a la situación política de entonces. Gracias al esfuerzo económico de sus socios y socias se logró alquilar una bajera. Gaztaroa ya tenía su sede social. Desde aquí queremos hacer llegar nuestro agradecimiento a todos los montañeros y montañeras que alguna vez habéis pasado por el club aportando vuestro granito de arena.\n
        Gracias!`,
  },
];

function RenderInfo(props) {
  
  const enviarEmail = async() => {
    let options = {
      subject: 'Dudas/Sugerencias',
      body: '',
      recipients: ['xaviusu@gmail.com'],
      isHtml: true
    };
    let promise = new Promise((resolve, reject) => {
      composeAsync(options)
        .then((result) => {
          resolve(result)
        })
        .catch((error) => {
          reject(error)
        })
      })
  }
  
  
  const contacto = INFO[0];
    if (contacto != null) {
      return (
        <Card>
          <Card.Title>{contacto.titulo}</Card.Title>
          <Card.Divider />
          <Text style={{ margin: 20, textAlign: "justify" }}>
            {contacto.descripcion}
          </Text>
          <Button onPress={() => enviarEmail()} title="Dudas/Sugerencias"></Button>
        </Card>
      );
    } else {
      return <View></View>;
    }
}
function RenderActividadesItem({ item, index }) {
  return (
    <ListItem
      key={index}
      bottomDivider>
      <Avatar source={{ uri: item.imagen }} />
      <ListItem.Content>
        <ListItem.Title>{item.nombre}</ListItem.Title>
        <ListItem.Subtitle>{item.descripcion}</ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );
};



class QuienesSomos extends Component {
  render() {
    if (this.props.actividades.isLoading) {
      return (
        <ScrollView>
          <RenderInfo />
          <Card>
            <Card.Title>"Actividades y recursos"</Card.Title>
            <Card.Divider />
            <IndicadorActividad />
          </Card>
        </ScrollView>
      );
    } else {
      return (
        <ScrollView>
          <RenderInfo />
          <Card>
            {this.props.actividades.actividades.map((item, index) => (
              <RenderActividadesItem key={index} item={item} index={index} />
            ))}
          </Card>
        </ScrollView>
      );
    }
    
  }
}

export default connect(mapStateToProps)(QuienesSomos);

