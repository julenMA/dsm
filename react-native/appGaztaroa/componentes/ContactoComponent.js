import React, { Component } from "react";
import { Text, View } from "react-native";
import { Card } from "react-native-elements";
import { CONTACTO } from "../comun/contacto";

function RenderContacto() {
  const contacto = CONTACTO[0];

  if (contacto != null) {
    return (
      <Card>
        <Card.Title>{contacto.titulo}</Card.Title>
        <Card.Divider />
        <Text
          style={{ margin: 20, textAlign: 'justify' }}
        >
          {contacto.descripcion}
        </Text>
      </Card>
    );
  } else {
    return <View></View>;
  }
}

class Contacto extends Component {
  render() {
    return <RenderContacto />;
  }
}

export default Contacto;
