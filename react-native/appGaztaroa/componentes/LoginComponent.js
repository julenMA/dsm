import {
    signInWithEmailAndPassword,
} from "firebase/auth";
import React, { Component, useState } from "react";
import { View, Pressable,StyleSheet, Text} from "react-native";
import { Input } from "react-native-elements";

import { connect } from 'react-redux';
import { auth } from "../firebaseDB";

const mapStateToProps = state => {
    return {

    } 
}

function RenderLogin(props) {

  const [usuario, setUsuario] = useState("");
  const [passwd, setPasswd] = useState("");

  const loginUsuario = async () => {
    alert("Ya esta logueado");
      const user = await signInWithEmailAndPassword(
        auth,
        usuario,
        passwd
      );
    };
    
    return (
        <View>
           <Input autoFocus
                    style={styles.input}
                    placeholder='Correo electrónico'
                    leftIcon={{ type: 'font-awesome', name: 'user' }}
                    leftIconContainerStyle={{ marginLeft: -30 }}
                    onChangeText={value => setUsuario(value)}
                    value={usuario}
                    name="email"
                  />
                  <Input autoFocus
                    style={{ marginLeft: '5%' }}
                    placeholder='Contraseña'
                    leftIcon={{ type: 'font-awesome', name: 'comment' }}
                    leftIconContainerStyle={{ marginLeft: -30 }}
                    onChangeText={value => setPasswd(value)}
                    value={passwd}
                    name="password"
                  />
                  <Pressable
                    style={[styles.button, styles.buttonSend]}
                    onPress={() => loginUsuario()}
                  >
                    <Text style={styles.textStyle}>Iniciar sesión</Text>
                  </Pressable>
            </View>
    );
}

class LoginComponent extends Component {
    render() {
        return (
            <View>
                <RenderLogin />
            </View>
        );
    }

  }

  const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      marginTop: 22
    },
    modalView: {
      backgroundColor: "white",
      borderRadius: 20,
      padding: 55,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    input: {
      height: '100%',
      margin: 12,
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonSend: {
      marginTop: '5%',
      backgroundColor: "#2196F3",
    },
    buttonClose: {
      marginTop: '5%',
      backgroundColor: "red",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
  });
export default connect(mapStateToProps)(LoginComponent);
