import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Campobase from "./componentes/CampobaseComponent";
import { Provider } from "react-redux";
import { ConfigureStore } from "./redux/configureStore";
import LottieView from "lottie-react-native";

const store = ConfigureStore();

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
    };
  }

  componentDidMount = async () => {
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 5000);
  };

  render() {
    if (this.state.isLoading) {
      return (
        <>
          <LottieView
            source={require("./assets/78783-mountain.json")}
            autoPlay
            loop
          />
          <View style={styles.container2}>
            <Text style={styles.steelblue}>App Gazteroa</Text>
          </View>
        </>
      );
    }
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Campobase />
          <StatusBar style="auto" />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  container2: {
    justifyContent: "flex-end", 
    alignItems: "center", 
    flex: 1,
    marginBottom: "25%",
  },
  steelblue: {
    textTransform: 'uppercase',
    fontWeight: "bold",
    color: "orange",
    fontSize: 25,
    textAlign: "center",
  },
});
