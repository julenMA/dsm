import db from "./firebase"
import {onSnapshot,collection,addDoc,doc,deleteDoc} from "firebase/firestore"

const obtenerFecha = () => {
    return new Date().toLocaleString() + ""
}

export const handleNew = async (precio, nombre) => {
    const fechaActual = obtenerFecha();
    const collectionRef = collection(db,"pedidos");
    const payload = { name: nombre, price: precio, date: fechaActual};
    await addDoc(collectionRef,payload);
        
}

export const handleDelete = async (id) => {
    const docRef = doc(db,"pedidos",id);
    await deleteDoc(docRef);
        
}
