import React from 'react'

const Greetings = (props) => {
    return (
        <div className="jumbotron text-center">
            <h1 className="display-3">Muchas Gracias!</h1>
            <p className="lead"><strong>Comprueba tu email</strong> o no, haz lo que quieras.</p>
            <hr />
            <p>
                Has tenido problemas? <a href="">No nos contactes</a>
            </p>
            <p className="lead">
                <a className="btn btn-primary btn-sm" onClick={() => props.goToPage('cart')} role="button">Volver a la pagina de inicio</a>
            </p>
        </div>
    )
}

export default Greetings