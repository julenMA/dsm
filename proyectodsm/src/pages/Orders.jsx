import { useState, useEffect } from 'react';
import db from "../firebase"
import { onSnapshot, collection } from "firebase/firestore"
import { handleDelete } from "../utils"
import { Button, Modal } from 'react-bootstrap';
import {
  onAuthStateChanged,
  signOut,
} from "firebase/auth";
import { auth } from "../firebase";

const Orders = (props) => {
  const [pedidos, setPedidos] = useState([]);
  const [idPedido, setIdPedidos] = useState();
  const [showModalEliminarPedido, setShowModalEliminarPedido] = useState(false);
  const [user, setUser] = useState({});

  const abrirModalEliminarPedido = () => {
    setShowModalEliminarPedido(true);
  }

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
    props.setUser(currentUser);
  });

  const salir = async () => {
    const user = await signOut(auth);
  }

  const handleCloseModalEliminarPedido = (value) => {
    if (value === 'confirmar') {
      handleDelete(idPedido);
    }
    setShowModalEliminarPedido(false);
  }

  useEffect(() => {
    onSnapshot(collection(db, "pedidos"), (snapshot) => {
      setPedidos(snapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    })
  })
  return (
    <>
      <div className="container-fluid py-3">
        <div className="row justify-content-center">
          <h4 className='text-center py-3 text-decoration-underline'>Lista de pedidos</h4>
          <div className="col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8 py-4">
            <div>
              <table className="table table-light table-hover m-0">
                <tbody>
                  {pedidos.map((item, index) => {
                    return (
                      <tr key={index} className='align-middle'>
                        <td>{"" + item.name}</td>
                        <td >${item.price}</td>
                        <td>{item.date}</td>
                        <td>
                          <button className='btn btn-outline-danger ms-5' onClick={() => { setIdPedidos(item.id); abrirModalEliminarPedido() }}>Eliminar pedido</button>
                        </td>
                      </tr>
                    )
                  })}
                </tbody>
              </table>
              {user !== null && (
              <Button className='m-0' onClick={salir}>
                    Cerrar sesion de {user?.email}
                  </Button>
              )}
            </div>
          </div>
        </div>
      </div>

      


      <Modal show={showModalEliminarPedido}>
        <Modal.Header closeButton onClick={() => handleCloseModalEliminarPedido('cerrar')}>
          <Modal.Title>Confirmacion</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => handleCloseModalEliminarPedido('cerrar')}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={() => handleCloseModalEliminarPedido('confirmar')}>
            Confirmar
          </Button>
        </Modal.Footer>
      </Modal>
    </>



  )
}

export default Orders