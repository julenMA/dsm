import Cards from '../components/Cards'
import './Home.css'
import { useState, useEffect } from 'react';
import { addToCart} from "../logic";
import db from "../firebase"
import {onSnapshot,collection, query, orderBy} from "firebase/firestore"

const Home = (props) => {

const [cart, setCart] = useState([]);
const [products, setProducts] = useState([]);

useEffect(() => {
  const collectionRef = collection(db,"products");
  const q = query(collectionRef, orderBy("id"));
  const unsub = onSnapshot(q,(snapshot) => {
    setProducts(snapshot.docs.map(doc => doc.data()));
  })
  return unsub;
}, [])

  return (
    <div className='container-fluid'>
        <h3 className='text-center mt-5 text-uppercase'>Shop Page</h3>
        <div className="container py-4">
            <div className="row">
                {products.map((item, index) => {
                    return(
                       <Cards img={item.img} title={item.title} desc={item.desc} price={item.price} key={index} addProduct={(value)=>{const newCart = addToCart(cart,value); setCart(newCart);props.actualizarCarro(newCart)}}/> 
                    )
                })}
            </div>
        </div>
    </div>
  )
}

export default Home