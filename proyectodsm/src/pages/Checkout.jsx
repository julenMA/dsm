import './Checkout.css';
import React, { useState, useRef } from 'react';
import { handleNew } from "../utils"
import { Alert} from 'react-bootstrap';
import PropTypes from 'prop-types';

const getTotalPrice = (sum) => {
    return sum + 2.99;
}




const Checkout = (props) => {
    const [nombre, setNombre] = useState();
    const [show, setShow] = useState(false);
    const inputRef = useRef();
    return (
        <>
            <Alert show={show} variant="danger" onClose={() => setShow(false)} dismissible>
                <Alert.Heading>Vaya! Parece que ha habido un error!</Alert.Heading>
                <p>
                    Compruebe que todos los campos están completados.
                </p>
            </Alert>
            <div className="row justify-content-center">
                <div className="col-lg-12">
                    <div className="card justify-content-center">
                        <div className="row justify-content-center">
                            <div className="col-lg-5">
                                <div className="row px-2">
                                    <div className="form-group col-md-6"> <label className="form-control-label">Nombre</label> <input type="text" ref={inputRef} onChange={(e) => setNombre(e.target.value)} placeholder="Johnny Doe" /> </div>
                                    <div className="form-group col-md-6"> <label className="form-control-label">Numero de tarjeta</label> <input type="text" placeholder="1111 2222 3333 4444" /> </div>
                                </div>
                                <div className="row px-2">
                                    <div className="form-group col-md-6"> <label className="form-control-label">Fecha de Expiracion</label> <input type="text" id="exp" name="exp" placeholder="MM/YYYY" /> </div>
                                    <div className="form-group col-md-6"> <label className="form-control-label">CVV</label> <input type="text" id="cvv" name="cvv" placeholder="***" /> </div>
                                </div>
                            </div>
                            <div className="col-lg-4 mt-2">
                                <div className="row d-flex justify-content-between px-4">
                                    <p className="mb-1 text-left">Subtotal</p>
                                    <h6 className="mb-1 text-right">${props.totalPrice}</h6>
                                </div>
                                <div className="row d-flex justify-content-between px-4">
                                    <p className="mb-1 text-left">Gastos de envio</p>
                                    <h6 className="mb-1 text-right">$2.99</h6>
                                </div>
                                <div className="row d-flex justify-content-between px-4" id="tax">
                                    <p className="mb-1 text-left">Total (impuestos incluidos)</p>
                                    <h6 className="mb-1 text-right">${getTotalPrice(props.totalPrice)}</h6>
                                </div> <button className="btn-block btn-blue" onClick={() => { if(nombre === '' || nombre ===undefined){setShow(true); inputRef.current.focus()}else{handleNew(getTotalPrice(props.totalPrice), nombre); props.realizarPedido() }}}> <span> <span id="checkout">Realizar Pedido</span> <span id="check-amt">${getTotalPrice(props.totalPrice)}</span> </span> </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            

        </>
    )
}

Checkout.propTypes = {
    props: PropTypes.array
  };

export default Checkout