import imgUno from './images/img1.jpg';
import imgDos from './images/img2.jpg';
import imgTres from './images/img3.jpg';
import imgCuatro from './images/img4.jpg';

const Data = {
    products: [
        {
            id: 1,
            img: imgUno,
            title: 'Producto Uno',
            desc: 'Descripcion',
            price: 80,
        },
        {
            id: 2,
            img: imgDos,
            title: 'Producto Dos',
            desc: 'Descripcion',
            price: 60,
        },
        {
            id: 3,
            img: imgTres,
            title: 'Producto Tres',
            desc: 'Descripcion',
            price: 50,
        },
        {
            id: 4,
            img: imgCuatro,
            title: 'Producto Cuatro',
            desc: 'Descripcion',
            price: 90,
        }
    ]
}
export default Data;    