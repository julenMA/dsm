


export const addToCart = (cart,product) => {
    let newCart = [...cart];
    let itemInCart = newCart.find(
      (item) => product.title === item.title);
    if (itemInCart) {
      itemInCart.quantity++;
    } else {
      itemInCart = {
        ...product,
        quantity: 1,
      };
      newCart.push(itemInCart);
    }
    return newCart;
}

export const removeFromCart = (cart,productToRemove) => {
 return (cart.filter((product) => product !== productToRemove));
}
export const clearCart = () => {
return [];
}

export const addQuantity = (cart,product) => {
    let newCart = [...cart];
    let itemInCart = newCart.find(
      (item) => product.title === item.title);
    itemInCart.quantity++;
    return(newCart);
}

export const reduceQuantity = (cart,product) => {
    let newCart = [...cart];
    let itemInCart = newCart.find(
      (item) => product.title === item.title);
    itemInCart.quantity--;
    if(itemInCart.quantity === 0){
      newCart = removeFromCart(newCart,product);
    }
    return(newCart);
}

export const getTotalSum = (cart) => {
 return cart.reduce((sum,{price, quantity}) => sum + price * quantity,0);
}