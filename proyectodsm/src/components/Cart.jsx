import './Cart.css';
import { useState } from 'react';
import { addQuantity, clearCart, reduceQuantity, removeFromCart, getTotalSum } from '../logic';
import { Button, Modal, Table } from 'react-bootstrap';
import imgUno from '../backend/images/img1.jpg';
import imgDos from '../backend/images/img2.jpg';
import imgTres from '../backend/images/img3.jpg';
import imgCuatro from '../backend/images/img4.jpg';

const Cart = (props) => {
  const cart = props.cart;

  const imageUrl = (param) => {
    switch (param) {
      case 'imgUno':
        return imgUno;
      case 'imgDos':
        return imgDos;
      case 'imgTres':
        return imgTres;
      case 'imgCuatro':
        return imgCuatro;
      default:
        return '';
    }
  }

  const getCartTotal = () => {
    return cart.reduce((sum, { quantity }) => sum + quantity, 0);
  }

  const [show, setShow] = useState(false);

  const abrirModalConfirmacion = () => {
    setShow(true);
  }

  const handleClose = (value) => {
    if (value === 'confirmar'){
      if (props.user !== null){
        props.goToPage('checkout');
      }else{
        props.goToPage('signup');
      }
    }
    setShow(false);
  }

  return (
    <>
      <div className="container-fluid py-3">
        <div className="row justify-content-center">
          <h4 className='text-center py-3 text-decoration-underline'>Mi Carro de la Compra</h4>
          <div className="col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8 py-4">
            <div className="d-flex justify-content-center py-3">
              <p className='position-relative fw-bolder text-title fs-6'>Carro <span className=' translate-middle rounded-pill badge bg-danger mx-1'>{cart.length}</span></p>
              <p className='fw-bolder text-title fs-6'>Total de unidades <span className=' translate-middle rounded-pill badge bg-success mx-1'>{getCartTotal()}</span></p>
            </div>
            <div>
              <Table className="table table-light " responsive="xxl">
                <tbody>
                  {cart.map((item, index) => {
                    return (
                      <tr key={index} className='align-middle'>
                        <td><img src={imageUrl(item.img)} className='img-cart' alt={item.title} /></td>
                        <td>{item.title}</td>
                        <td>${item.price}</td>
                        <td>Cantidad: {item.quantity}</td>
                        <td>
                          <button className='btn btn-outline-dark ms-1' onClick={() => { const newCart = reduceQuantity(cart, item); props.actualizarCarro(newCart) }}>-</button>
                          <button className='btn btn-outline-dark ms-1' onClick={() => { const newCart = addQuantity(cart, item); props.actualizarCarro(newCart) }}>+</button>
                          <button className='btn btn-outline-danger ms-5' onClick={() => { const newCart = removeFromCart(cart, item); props.actualizarCarro(newCart) }}>Eliminar de la cesta</button>
                        </td>
                      </tr>
                    )
                  })}
                </tbody>
              </Table>
            </div>
            <div className="d-flex justify-content-between py-3">
              {cart.length > 0 && (
                <button className='btn btn-outline-danger' onClick={() => { const newCart = clearCart(cart); props.actualizarCarro(newCart) }}>Vaciar carro</button>
              )}
              <h3>Precio total: ${getTotalSum(cart)}</h3>
            </div>
            <div className="d-flex justify-content-end">
              <button type="button" className='btn btn-primary' onClick={() => abrirModalConfirmacion()}>Realizar Pedido</button>
            </div>
          </div>
        </div>
      </div>



      <Modal show={show}>
        <Modal.Header closeButton onClick={() => handleClose('cerrar')}>
          <Modal.Title>Confirmacion</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <table>
            <tbody>
              {cart.map((item, index) => {
                return (
                  <tr key={index}>
                    <td><img src={imageUrl(item.img)} className='img-cart' alt={""} /></td>
                    <td>{item.quantity} x {item.title}</td>
                    <td >: ${item.price * item.quantity}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
          <div className="d-flex justify-content-between py-3">
            <h3>Precio total: ${getTotalSum(cart)}</h3>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => handleClose('cerrar')}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={() => handleClose('confirmar')}>
            Confirmar Pedido
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default Cart