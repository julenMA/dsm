import React, { useRef, useState } from "react";
import { Card, Button, Form, Alert } from "react-bootstrap";
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";
import { auth } from "../firebase";

const Signup = (props) => {
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const emailLogInRef = useRef();
  const passwordLogInRef = useRef();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState({});
  const [booleano, setBooleano] = useState(false);

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
    props.setUser(currentUser);
  });

  async function handleSubmit(e) {
    e.preventDefault();
    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Las contraseñas no coinciden");
    }
    try {
      setError("");
      setLoading(true);
      await registrarUsuario();
    } catch {
      setError("Fallo al crear la cuenta");
    }
    setLoading(false);
  }

  async function handleSubmitLogIn(e) {
    e.preventDefault();

    try {
      setError("");
      setLoading(true);
      await loginUsuario();
      props.goToPage('products')
    } catch {
      setError("Fallo al iniciar sesion");
    }
    setLoading(false);
  }

  const registrarUsuario = async () => {
    const user = await createUserWithEmailAndPassword(
      auth,
      emailRef.current.value,
      passwordRef.current.value
    );
  };

  const loginUsuario = async () => {
    const user = await signInWithEmailAndPassword(
      auth,
      emailLogInRef.current.value,
      passwordLogInRef.current.value
    );
  };

  const salir = async () => {
      const user = await signOut(auth);
  }

  return (
    <>
      <Card>
        <Card.Body>
          {booleano === false && (
            <h2 className=" text-center mb-4">Crear cuenta</h2>
          )}
          {booleano === true && (
            <h2 className=" text-center mb-4">Inicia sesion</h2>
          )}
          {error && <Alert variant="danger">{error}</Alert>}
          {booleano === false && (
            <Form >
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  ref={emailRef}
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group id="password">
                <Form.Label>Contraseña</Form.Label>
                <Form.Control
                  type="password"
                  ref={passwordRef}
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group id="password-confirm">
                <Form.Label>Repite la Contraseña</Form.Label>
                <Form.Control
                  type="password"
                  ref={passwordConfirmRef}
                  required
                ></Form.Control>
              </Form.Group>
              <Button disabled={loading} onClick={handleSubmit} className="w-100" type="submit">
                Crear cuenta
              </Button>
            </Form>
          )}
          {booleano === true && (
            <Form>
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  ref={emailLogInRef}
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group id="password">
                <Form.Label>Contraseña</Form.Label>
                <Form.Control
                  type="password"
                  ref={passwordLogInRef}
                  required
                ></Form.Control>
              </Form.Group>
              <Button disabled={loading} onClick={handleSubmitLogIn} className="w-100" type="submit">
                Iniciar Sesion
              </Button>
            </Form>
          )}
        </Card.Body>
      </Card>
      {booleano === false && (
        <div className="w-100 text-center mt-2">
          ¿Ya tienes una cuenta?{" "}
          <Button onClick={() =>setBooleano(true)} >
            Entra
          </Button>
        </div>
      )}
      {booleano === true && (
        <div className="w-100 text-center mt-2">
          ¿No tienes una cuenta?{" "}
          <Button onClick={() =>setBooleano(false)} >
            Crea una
          </Button>
        </div>
      )}
      {user !== null && (
      <Button onClick={salir}>
            {user?.email}
          </Button>
          )}
    </>
  );
};

export default Signup;
