import './Cards.css'
import imgUno from '../backend/images/img1.jpg';
import imgDos from '../backend/images/img2.jpg';
import imgTres from '../backend/images/img3.jpg';
import imgCuatro from '../backend/images/img4.jpg';

const Cards = (props) => {

    const imageUrl = (param) => {
        switch (param) {
            case 'imgUno':
                return imgUno;
            case 'imgDos':
                return imgDos;
            case 'imgTres':
                return imgTres;
            case 'imgCuatro':
                return imgCuatro;
            default:
                return '';
        }
    }

    return (
        <div className='col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 col-xxl-3 mb-4'>
            <div className="card h-100 shadow">
                <img src={imageUrl(props.img)} alt='' className='card-img-top img-fluid' />
                <div className="card-body">
                    <div className="d-flex justify-content-between">
                        <h5 className='card-title'>{props.title}</h5>
                        <span className='fw-bolder'>${props.price}</span>
                    </div>
                    <div className="">
                        <p className='card-text'>{props.desc}</p>
                    </div>
                    <div className="d-grid justify-content-end mt-4">
                        <button className='btn btn-sm btn-outline-success' onClick={() => props.addProduct(props)}>Añadir</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Cards