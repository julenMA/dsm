import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import Home from './pages/Home'
import Cart from './components/Cart'
import { useState } from 'react';
import Checkout from './pages/Checkout';
import { Container,  Nav, Navbar} from 'react-bootstrap';
import Orders from './pages/Orders';
import Greetings from './pages/Greetings';
import { getTotalSum } from './logic';
import Signup from './components/Signup';
import {
  onAuthStateChanged
} from "firebase/auth";
import { auth } from "./firebase";

function App() {
  const [page, setPage] = useState('products')
  const [cart, setCart] = useState([]);
  const [user,setUser] = useState();
  
  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });

  const goToPage = (pageToGo) => {
    setPage(pageToGo);
  }
  const actualizarCarro = (newCart) => {
    setCart(newCart);
  }

  return (
    <>
      <Navbar bg="light" collapseOnSelect >
        <Container fluid>
          <Navbar.Brand onClick={() => goToPage('products')}>Lista de Productos</Navbar.Brand>
          <Nav>
            <p className='p-3' onClick={() =>{if(user !== null && user !== undefined ){goToPage('pedidos')}else{goToPage('signup') }}}>Pedidos</p>
            
            <p onClick={() => goToPage('cart')} className='p-3 position-relative fw-bolder text-title fs-6'>Carro <span className=' translate-middle rounded-pill badge bg-danger mx-1'>{cart.length}</span></p>
            
          </Nav>
        </Container>
      </Navbar>
      <div >
        {page === 'products' && (
          <Home actualizarCarro={value => actualizarCarro(value)}/>
        )}
        {page === 'cart' && (
          <Cart user={user} cart={cart} actualizarCarro={value => actualizarCarro(value)} goToPage={(value)=>goToPage(value)} />
        )}
        {page === 'checkout' && (
          <Checkout totalPrice={getTotalSum(cart)} realizarPedido={() => {goToPage('terminado');actualizarCarro([])}}/>
        )}
        {page === 'pedidos' && (
          <Orders setUser={(value) =>setUser(value)} />
        )}
        {page === 'signup' && (
          <Container className='d-flex align-items-center justify-content-center' style={{minHeight: "100vh"}}>
            <div className='w-100' style={{ maxWidth: '400px'}}>
              <Signup setUser={(value) =>setUser(value)} goToPage={(value)=>goToPage(value)}/>
            </div>
            
          </Container>
          
        )}
        {page === 'terminado' && (
          <Greetings goToPage={() => goToPage('products')}/>
        )}
      </div>
    </>
  );
}

export default App;
