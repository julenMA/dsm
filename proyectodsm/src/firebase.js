// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getFirestore} from "firebase/firestore";
import {getAuth} from 'firebase/auth'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCENr2pIuAe3UvwDcruBny99I7Cj7zCgU4",
  authDomain: "fb-dsm.firebaseapp.com",
  projectId: "fb-dsm",
  storageBucket: "fb-dsm.appspot.com",
  messagingSenderId: "916469510528",
  appId: "1:916469510528:web:b2f85310305d7e8b595165"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)
export default getFirestore();