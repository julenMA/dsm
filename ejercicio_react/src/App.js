import logo from './logo.svg';
import './App.css';
import Entrada from './components/Entrada';
import Salida from './components/Salida';
import Multiplos from './components/Multiplos';
import { useState } from 'react';

function App() {
  const[selectedMultiplo1, setSelectedMultiplo1] = useState('');
  const[selectedMultiplo2, setSelectedMultiplo2] = useState('');
  return (
    <div className="App">
      <div className='container mt-5'>
          <Entrada onChange={value => setSelectedMultiplo1(value)}/> 
          <Multiplos setSelectedMultiplo2={setSelectedMultiplo2}/> 
          <Salida valor={mul(selectedMultiplo1,selectedMultiplo2)}/>  
      </div>
    </div>
  );
}

function mul(a,b) {
  return a * b;
}

export default App;
