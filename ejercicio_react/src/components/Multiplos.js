import React from "react";
import { useState } from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';

const Multiplos = ({setSelectedMultiplo2}) =>{

    const [radioValue, setRadioValue] = useState('1');
    const radios = [
        { name: '37', value: 37 },
        { name: '43', value: 43 },
      ];

    const onButtonClick=(Multiplo)=>{
        setSelectedMultiplo2(Multiplo)
    }

    return (
    <>
        <h1>Multiplicado por</h1>
        <ButtonGroup>
            {radios.map((radio, idx) => (
            <ToggleButton
                key={idx}
                id={`radio-${idx}`}
                type="radio"
                variant={idx % 2 ? 'outline-success' : 'outline-danger'}
                name="radio"
                value={radio.value}
                checked={radioValue === idx}
                onChange={(e) => setRadioValue(idx)}
                onClick={(e)=>onButtonClick(radio.value)}
            >
                {radio.name}
            </ToggleButton>
            ))}
        </ButtonGroup>
    </>
    
    )
};

export default Multiplos;