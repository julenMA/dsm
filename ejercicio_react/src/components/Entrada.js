import React,{Fragment,useState, Component} from "react";

const Entrada = (props) =>{

    return (
        <div className="justify-content-center">
            <h1>El numero</h1>
            <form className="col-lg-2 offset-lg-5">
                <div className="row justify-content-center">
                    <input 
                        placeholder="Introduzca un numero"
                        className="form-control "
                        type="number"
                        onChange={(event)=>props.onChange(event.target.value)}
                    />
                </div>
            </form>
        </div>
    );
}

export default Entrada;